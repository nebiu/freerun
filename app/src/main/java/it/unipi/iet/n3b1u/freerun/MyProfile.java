package it.unipi.iet.n3b1u.freerun;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class MyProfile extends AppCompatActivity {

    private Button editButton;
    String user_data[] = new String[5];
    final String FILENAME = "user_data.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        readUserDataFromFile();
        editButton = (Button) findViewById(R.id.editBtn);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEditProfile();
            }
        });
    }

    public void goToEditProfile() {
        Intent intentEdit = new Intent(this, EditProfile.class);
        startActivity(intentEdit);
    }

    public void readUserDataFromFile(){

        TextView name = (TextView) findViewById(R.id.name);
        TextView gender = (TextView) findViewById(R.id.myGender);
        TextView dob = (TextView) findViewById(R.id.myDob);
        TextView height = (TextView) findViewById(R.id.myHeight);
        TextView weight = (TextView) findViewById(R.id.myWeight);

        // Read from file, put in array, take strings from vector to display in the layout
        BufferedReader input = null;
        try{
            input = new BufferedReader(new InputStreamReader(openFileInput(FILENAME)));
            for (int i=0; i < user_data.length; i++)
                user_data[i] = input.readLine();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (input != null)
                try {
                    input.close();
                } catch (Exception e){
                    e.printStackTrace();
                }
        }
        name.setText(user_data[0]);
        gender.setText(user_data[1]);
        dob.setText(user_data[2]);
        height.setText(user_data[3]);
        weight.setText(user_data[4]);
    }
}
