package it.unipi.iet.n3b1u.freerun.utils;

public class ActivityRecord {

    private String date, distance, time, speed, calories;


    public ActivityRecord(String date, String distance, String time, String speed, String calories) {
        this.date = date;
        this.distance = distance;
        this.time = time;
        this.speed = speed;
        this.calories = calories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getDistance() {
        return distance;
    }

    public String getTime() {
        return time;
    }

    public String getSpeed() {
        return speed;
    }

    public String getCalories() {
        return calories;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }
}
