package it.unipi.iet.n3b1u.freerun;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.unipi.iet.n3b1u.freerun.database.DatabaseHelper;
import it.unipi.iet.n3b1u.freerun.utils.ActivityRecord;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {


    private List<ActivityRecord> mDataset;
    DatabaseHelper myDb;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView dateTV, distanceTV, timeTV, speedTV, calTV;

        //create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            dateTV = (TextView) itemView.findViewById(R.id.date);
            distanceTV = (TextView) itemView.findViewById(R.id.DisNum);
            timeTV = (TextView) itemView.findViewById(R.id.TimeNum);
            speedTV = (TextView) itemView.findViewById(R.id.SpeedNum);
            calTV = (TextView) itemView.findViewById(R.id.CalNum);
        }
    }


    // Provide a constructor
    public MyAdapter(List<ActivityRecord> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View recordListView = inflater.inflate(R.layout.card, parent, false);
        // Return a new holder instance
        MyAdapter.ViewHolder viewHolder = new MyAdapter.ViewHolder(recordListView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ActivityRecord record = mDataset.get(position);
        holder.dateTV.setText(record.getDate());
        holder.distanceTV.setText(String.format("%.3f", Float.parseFloat(record.getDistance())));
        holder.timeTV.setText(String.format("%.2f", Float.parseFloat(record.getTime())));
        holder.speedTV.setText(String.format("%.2f", Float.parseFloat(record.getSpeed())));
        holder.calTV.setText(String.format("%.2f", Float.parseFloat(record.getCalories())));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }



}
