package it.unipi.iet.n3b1u.freerun.utils;


import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;


public class PointManager {
    public static String TAG = "PointManager";
    private static final int TRAIL_SIZE = 100;
    private List<Point> queue = new ArrayList<Point>();


    public void insert(Point p) {
        if (!queue.contains(p)){
            queue.add(p);
            if(queue.size() > TRAIL_SIZE)
                queue.remove(0);
        }
    }

    public PolylineOptions getPolylineOptions(){
        PolylineOptions po = new PolylineOptions();

        for(Point p: queue)
            po.add(new LatLng(p.getLatitude(), p.getLongitude()));

        po.color(Color.RED);
        return po;
    }

    public List<CircleOptions> getPositions(){

        List<CircleOptions> co = new ArrayList<CircleOptions>();

        for(Point p: queue)
            co.add(new CircleOptions()
                    .center(new LatLng(p.getLatitude(), p.getLongitude()))
                    .radius(0.0)
                    .strokeColor(Color.BLUE));

        return co;
    }

    public MarkerOptions getLastPositionMarker(){

        if(queue.size() == 0)
            return null;

        MarkerOptions mo = new MarkerOptions();
        Point last = getLastPoint();
        mo.position(new LatLng(last.getLatitude(), last.getLongitude()));
        return mo;
    }
    public Point getLastPoint(){

        if(queue.size() == 0)
            return null;

        Point last = queue.get(queue.size()-1);
        return last;
    }

    public Point getFirstPoint(){

        if(queue.size() == 0)
            return null;

        return queue.get(0);
    }

    public void deletePoints(){
        queue.clear();
    }
}