package it.unipi.iet.n3b1u.freerun;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.DatePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class EditProfile extends AppCompatActivity  {

    private Button dateBtn;
    private Button confirmButton;
    int y, m, d;
    static final int DIALOG_ID = 0;
    String user_data[] = new String[5];
    final String FILENAME = "user_data.txt";
    final Calendar cal = Calendar.getInstance();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        showDialogOnClick();


        confirmButton = findViewById(R.id.btnConfirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeUserDataInFile();
                goToMain();
            }
        });

    }

    public void showDialogOnClick(){
        dateBtn = (Button) findViewById(R.id.dateBtn);
        dateBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );

    }

    @Override
    protected Dialog onCreateDialog(int id){

        if (id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, y, m, d);
        return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListener
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            y = year;
            m = month+1;
            d = dayOfMonth;
            Toast.makeText(EditProfile.this, d + "/" + m + "/" + y, Toast.LENGTH_LONG).show();
            EditText editText = (EditText) findViewById(R.id.showDate);
            editText.setText(d + "/" + m + "/" + y);
        }
    };

    public void goToMain() {
        Intent intent1 = new Intent(this, MainActivity.class);
        startActivity(intent1);
    }

    public void writeUserDataInFile(){

        // Take user input
        final EditText name = (EditText) findViewById(R.id.yourname);
        final EditText birthdate = (EditText) findViewById(R.id.showDate);
        final EditText height = (EditText) findViewById(R.id.yheight);
        final EditText weight = (EditText) findViewById(R.id.yweight);

        RadioGroup genderRadio = (RadioGroup) findViewById(R.id.genderRadio);
        int selectedId = genderRadio.getCheckedRadioButtonId();
        final RadioButton gender = (RadioButton) findViewById(selectedId);
        // Save user data in an array
        user_data[0] = name.getText().toString();
        user_data[1] = gender.getText().toString();
        user_data[2] = birthdate.getText().toString();
        user_data[3] = height.getText().toString();
        user_data[4] = weight.getText().toString();

        // Write array to a file
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(openFileOutput(FILENAME, MODE_PRIVATE)));
            for (int i=0; i<user_data.length; i++){
                writer.write(user_data[i]);
                writer.write("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Please set up your profile before starting your activities!", Toast.LENGTH_LONG).show();
            goToMain();
        } finally {
            if (writer !=null){
                try{
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
