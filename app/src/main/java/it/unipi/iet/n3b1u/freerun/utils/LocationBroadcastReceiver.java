package it.unipi.iet.n3b1u.freerun.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

public abstract class LocationBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "LocationBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(LocationManager.KEY_LOCATION_CHANGED)) {
            Location location = (Location)
                    intent.getExtras().get(LocationManager.KEY_LOCATION_CHANGED);
            onLocationChanged(context, location);
        } else if (intent.hasExtra(LocationManager.KEY_PROVIDER_ENABLED)) {
            if (intent.getExtras().getBoolean(LocationManager.KEY_PROVIDER_ENABLED)) {
                onProviderEnabled(null);
            } else {
                onProviderDisabled(null);
            }
        } else if (intent.hasExtra(LocationManager.KEY_PROXIMITY_ENTERING)) {
            if (intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, false)) {
                onEnteringProximity(context);
            } else {
                onExitingProximity(context);
            }
        } else if (intent.hasExtra(LocationManager.KEY_STATUS_CHANGED)) {
            onStatusChanged();
        }
    }

    public void onLocationChanged(Context context, Location location) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    public void onEnteringProximity(Context context) {
    }

    public void onExitingProximity(Context context) {
    }

    public void onStatusChanged() {
    }
}