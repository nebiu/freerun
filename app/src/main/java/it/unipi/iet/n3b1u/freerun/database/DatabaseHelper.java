package it.unipi.iet.n3b1u.freerun.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import it.unipi.iet.n3b1u.freerun.utils.ActivityRecord;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Database.db";

    public static final String TABLE_NAME = "ACTIVITY";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_DATE = "DATE";
    public static final String COLUMN_DISTANCE = "DISTANCE";
    public static final String COLUMN_TIME = "TIME";
    public static final String COLUMN_SPEED = "SPEED";
    public static final String COLUMN_CALORIES = "CALORIES";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME );

        String query = "create table " + TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_DISTANCE + " REAL, " +
                COLUMN_TIME + " REAL, " +
                COLUMN_SPEED + " REAL, " +
                COLUMN_CALORIES + " REAL" + ")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addActivity(double distance, double time, double speed, double calories){

        SQLiteDatabase db = getWritableDatabase();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        String date =  String.valueOf(formatter.format(d));

        ContentValues values = new ContentValues();
        //values.put(COLUMN_ID, id);
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_DISTANCE, distance);
        values.put(COLUMN_TIME, time);
        values.put(COLUMN_SPEED, speed);
        values.put(COLUMN_CALORIES, calories);
        try{
            long result = db.insert(TABLE_NAME, null, values);
            if (result == -1)
                return false;
            else
                return true;

        } finally {
            db.close();
        }

    }

    public ArrayList<ActivityRecord> getAllData(){
        ArrayList<ActivityRecord> activityRecords = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);

        while (res.moveToNext()){
            String date = res.getString(1);
            String dist = res.getString(2);
            String time = res.getString(3);
            String speed = res.getString(4);
            String cal = res.getString(5);

            ActivityRecord activityRecord = new ActivityRecord(date, dist, time, speed, cal);
            activityRecords.add(activityRecord);
        }
        return activityRecords;
    }

}
