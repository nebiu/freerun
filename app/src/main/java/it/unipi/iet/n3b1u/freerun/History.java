package it.unipi.iet.n3b1u.freerun;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.common.data.DataBuffer;

import java.util.ArrayList;
import java.util.List;

import it.unipi.iet.n3b1u.freerun.database.DatabaseHelper;
import it.unipi.iet.n3b1u.freerun.utils.ActivityRecord;

public class History extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        List<ActivityRecord> activityRecords = new ArrayList<ActivityRecord>();
        DatabaseHelper myDb = new DatabaseHelper(this);

        activityRecords = myDb.getAllData();
        mAdapter = new MyAdapter(activityRecords);
        mRecyclerView.setAdapter(mAdapter);

    }
}
