package it.unipi.iet.n3b1u.freerun;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import it.unipi.iet.n3b1u.freerun.database.DatabaseHelper;
import it.unipi.iet.n3b1u.freerun.utils.LocationBroadcastReceiver;
import it.unipi.iet.n3b1u.freerun.utils.Point;
import it.unipi.iet.n3b1u.freerun.utils.PointManager;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;



@RequiresApi(api = Build.VERSION_CODES.O)
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    protected LocationManager locationManager;
    private PointManager pointManager;



    private Button startBtn;
    long MillisecondTime, StartTime = 0L ;
    int Hours, Seconds, Minutes ;
    double totDist = 0.0, kmhSpeed, hoursTime, distKm, distance, cals=0;
    TextView stopwatch, speed, dist, cal;
    Handler handler;


    private static final int REQUEST_CODE = 0;
    private static final String ADD_LOCATION_ACTION =
            "it.unipi.iet.smp.ACTION_LOCATION_CHANGED";

    private PendingIntent pendingIntent;
    private UpdateBroadcastReceiver ubr;
    String user_data[] = new String[5];
    final String FILENAME = "user_data.txt";
    DatabaseHelper myDb;



    private int weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        myDb = new DatabaseHelper(getApplicationContext());

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        ubr = new UpdateBroadcastReceiver();

        pointManager = new PointManager();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //stopwatch
        stopwatch = (TextView) findViewById(R.id.tvTimer);
        speed = (TextView) findViewById(R.id.tvSpeed);
        dist = (TextView) findViewById(R.id.tvDistance);
        cal = (TextView) findViewById(R.id.tvCalories);

        // read data to calculate calories
        // Read from file, put in array, take strings from vector to display in the layout


            BufferedReader input = null;
            try{
                input = new BufferedReader(new InputStreamReader(openFileInput(FILENAME)));
                for (int i=0; i < user_data.length; i++)
                    user_data[i] = input.readLine();

            } catch (Exception e){
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Please set up your profile before starting your activities!", Toast.LENGTH_LONG).show();
                goToMainActivity();
            } finally {
                if (input != null)
                    try {
                        input.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }




        //name - user_data[0]
        //gender - user_data[1]
        //dob - user_data[2]
        //height - user_data[3]
        //weight - user_data[4]

        String weightStr = user_data[4];
        weight = Integer.parseInt(weightStr);



        startBtn = (Button) findViewById(R.id.btnStart);
        startBtn.setTag(1);

        handler = new Handler();

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int status = (Integer) v.getTag();

                if (status == 1 ){
                    pendingIntent = createPendingIntent();
                    registerReceiver(ubr, new IntentFilter(ADD_LOCATION_ACTION));
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    for (String provider : locationManager.getProviders(criteria, true)) {
                        locationManager.requestLocationUpdates(provider, 0, 0,pendingIntent);
                    }
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                    startBtn.setText("End Activity");
                    startBtn.setTag(0);


                } else {

                    boolean isInserted = myDb.addActivity( distance, hoursTime,  kmhSpeed,  cals);

                    if (isInserted)
                        Toast.makeText(getApplicationContext(), "Activity saved in the database", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getApplicationContext(), "Activity not saved correclty!", Toast.LENGTH_LONG).show();


                    locationManager.removeUpdates(pendingIntent);
                    unregisterReceiver(ubr);
                    goToMainActivity();

                }

            }
        });
    }


    public void goToMainActivity() {
        Intent intent0 = new Intent(this, MainActivity.class);
        startActivity(intent0);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("End Activity without saving?");
        alertDialogBuilder
                //.setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                goToMainActivity();                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            Seconds = (int) (MillisecondTime / 1000);
            Minutes = Seconds / 60;
            Hours = Minutes / 60;
            Seconds = Seconds % 60;
            Minutes = Minutes % 60;

            hoursTime = MillisecondTime/3600000.0;
            stopwatch.setText("" + Hours + ":" + String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));

            distKm = (totDist)/1000.0; //  divide by 1000 to get distance in km
            distance = distKm;
            dist.setText("" + String.format("%.3f", distKm));


            kmhSpeed = distKm*3600000/MillisecondTime;
            speed.setText("" + String.format("%.2f", kmhSpeed));

            cals =  distKm * weight * 1.036;
            cal.setText("" + String.format("%.2f", cals));


            handler.postDelayed(this, 0);
        }

    };



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
        }


        FusedLocationProviderClient mFusedLocationClient = getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location == null)
                            return;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                                .zoom(16)                   // Sets the zoom
                                //.bearing(90)                // Sets the orientation of the camera to east
                                //.tilt(30)                   // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder

                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "This application needs to access your location", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }


    public void onFilteredLocationChanged(Location l) {
        Point point = new Point(l.getLatitude(), l.getLongitude(),
                l.getAccuracy(), l.getTime(), l.getProvider());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(
                new LatLng(point.getLatitude(), point.getLongitude())));
        pointManager.insert(point);
        mMap.clear();
        PolylineOptions po = pointManager.getPolylineOptions();
        List<CircleOptions> co = pointManager.getPositions();
        mMap.addPolyline(po);
        for (CircleOptions c : co)
            mMap.addCircle(c);
        MarkerOptions mo = pointManager.getLastPositionMarker();
        if (mo != null)
            mMap.addMarker(mo);
    }


    private class UpdateBroadcastReceiver extends LocationBroadcastReceiver {
        private static final String TAG = "UpdeteBroadcastReceiver";
        private static final int TIME_THRESHOLD = 30000;// (ms)
        private static final int ACCURACY_FRACTION = 10;
        private static final int VELOCITY_THRESHOLD = 200;

        @Override
        public void onLocationChanged(Context context, Location location) {
            Log.i("UpdateBroadcastReceiver", "new location was detected");
            Point lastPoint = pointManager.getLastPoint();
            if (lastPoint == null) {
                onFilteredLocationChanged(location);
            } else {
                float currentAccuracy = location.getAccuracy();
                float previousAccuracy = lastPoint.getAccuracy();
                float accuracyDifference = Math.abs(previousAccuracy - currentAccuracy);
                boolean betterAccuracy = currentAccuracy < previousAccuracy;
                boolean lowerAccuracyAcceptable = !betterAccuracy
                        && lastPoint.getProvider().equals(location.getProvider())
                        && (accuracyDifference <= previousAccuracy / ACCURACY_FRACTION);
                float[] results = new float[1];
                Location.distanceBetween(lastPoint.getLatitude(), lastPoint.getLongitude(),
                        location.getLatitude(), location.getLongitude(),
                        results);

                float velocity = results[0] / ((location.getTime() - lastPoint.getTime()) / 1000);
                if (velocity <= VELOCITY_THRESHOLD
                        && (betterAccuracy
                        || (location.getTime() - lastPoint.getTime()) > TIME_THRESHOLD
                        || lowerAccuracyAcceptable)) {
                    totDist = totDist + results[0];
                    onFilteredLocationChanged(location);
                } else {
//Ignore point
                }
            }
        }
    }

    private PendingIntent createPendingIntent() {
        Intent intent = new Intent(ADD_LOCATION_ACTION);
        return PendingIntent.getBroadcast(getApplicationContext(),
                REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
