package it.unipi.iet.n3b1u.freerun;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static android.os.Process.myPid;

public class MainActivity extends AppCompatActivity {

    private Button button0;
    private Button button1;
    private Button button2;
    String user_name;
    final String FILENAME = "user_data.txt";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



                BufferedReader input = null;

        try{
            input = new BufferedReader(new InputStreamReader(openFileInput(FILENAME)));
            user_name = input.readLine();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (input != null)
                try {
                    input.close();
                } catch (Exception e){
                    e.printStackTrace();
                }
        }

        TextView name = (TextView) findViewById(R.id.nameView);
        if (user_name != null){
            name.setText("Hello " + user_name);
        } else{
            name.setText("Hello");
        }


        button0 = findViewById(R.id.btnNewActivity);
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMapsActivity();
            }
        });

        button1 = findViewById(R.id.btnProfile);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProfile();
            }
        });

        button2 = findViewById(R.id.btnHistory);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHistory();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("EXIT", true);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void goToMapsActivity() {
        Intent intent0 = new Intent(this, MapsActivity.class);
        startActivity(intent0);
    }
    public void goToProfile() {
        Intent intent1 = new Intent(this, MyProfile.class);
        startActivity(intent1);
    }

    public void goToHistory() {
        Intent intent2 = new Intent(this, History.class);
        startActivity(intent2);
    }
}
