package it.unipi.iet.n3b1u.freerun.utils;


import java.util.Objects;

public class Point {
    private double latitude;
    private double longitude;
    private float accuracy;
    private long time;
    private String provider;

    public Point(double lat, double lon, float acc, long time, String prov) {
        this.latitude = lat;
        this.longitude = lon;
        this.accuracy = acc;
        this.provider = prov;
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public long getTime() {
        return time;
    }

    public String getProvider() {
        return provider;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.latitude, latitude) == 0 &&
                Double.compare(point.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
}